# Moises Sacal Apr/2016
#
# This script extracts soilnm from each scenario_[A|B|C][0:10]
#
# Source this script to execute main(dayCentPath, "Scenario","A")

scenario = function(fileName) {
  table = read.table(fileName, header = TRUE)
  #print(dim(table)[1])
  vec = vector()
  for(index in 1:dim(table)[1]) {
    if(grepl("\\d{4}.67", table$time[index]) && (table$time[index] >= 1905 && table$time[index] < 2014)) {
      #print(paste(table$time[index], table$soilnm[index], sep=" "))
      #print(paste(table$soilnm[index], " - ", table$soilnm[index - 1]))
      vec = c(vec, table$soilnm[index] - table$soilnm[index - 1])
    }
  }
  return(vec)
}

main = function(dirPath, currentM, modelType) {
  yearCol = vector()
  for(i in 1905:2013) {
    yearCol = c(yearCol, i)
  }
  frame = data.frame(year = yearCol)
  colnaming = c("years")
  for(index in 0:10) {
    if(index < 10){m = sprintf("0%d", index)} else {m = sprintf("%d", index)}
    fileName = paste(currentM, "_", tolower(modelType), m, ".lis", sep = "")
    scenarioPath = file.path(dirPath, paste(currentM, modelType, sep=""), fileName)
    vec = vector()
    vec = scenario(scenarioPath) 
    colnaming = c(colnaming, paste(currentM, "_", modelType, m, sep=""))
    frame = cbind(frame, vec)
  }
  colnames(frame) = colnaming
  write.csv(frame, file = file.path(dirPath, paste(currentM, modelType, "_lis_soilNM.csv", sep = "")), row.names = FALSE)
}

#dirPath is the subpath containing all M[A|B|C] folders
#dayCentPath = file.path("~","RProjects","ensemble01","data", "Daycent runs")
dayCentPath = file.path("C:","Users","sacalbon","daycent", "result")
main(dayCentPath, "Scenario","A")
main(dayCentPath, "Scenario","B")
main(dayCentPath, "Scenario","C")
