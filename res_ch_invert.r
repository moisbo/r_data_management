# Moises Sacal Apr/2016
# 
# Script that modifies .RES files in a directory switching the chamber (chA) name and the sample [1:4] 
# This version will just invert the sample-chamber combo

modifyLines = function(dirPath) {
  #find .RES files
  files = list.files(path = dirPath, pattern = "\\.RES$")
  for (fileName in files) {
    print(fileName)
    cFile = file.path(dirPath, fileName)
    aCsv = tryCatch({read.csv(cFile, header = F, skip = 21, stringsAsFactors = F)}
                    ,error = function(error) {
                      print(paste("Error in file", cFile))
                      aCsv = NULL
                    })
    if(!is.null(aCsv)) {
      for(index in 1:nrow(aCsv) ) {
        cs = toString(aCsv[[3]][index])
        if(!is.null(grepl("cs-\\d", cs))){ #because R
          if(!grepl("cs-\\d", cs)) {
            cs = strsplit(cs,"-")
            sample = cs[[1]][1]
            chamber = cs[[1]][2]
            aCsv[[3]][index] = paste(chamber, sample, sep="-")
          }
        }
      }
    }
    con = file(cFile, blocking = F)
    header = readLines(con)
    close(con)
    cat("", file = cFile, append = F, sep = "") #clear file
    for(i in 1:21){
      cat(header[[i]], file = cFile, append = T, sep = "\n")
    }
    write.table(aCsv, file = cFile, sep = ",", col.names = F, row.names = F, append = T)
  }
}

dirPath = file.path("~","RProjects","ensemble01","data", "RESCH4")
#dirPath = file.path("C:","Users","username","data") #Windows example
options(warn=1) #show warnings
modifyLines(dirPath)
