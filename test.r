#!/usr/local/bin/R
abc = function(poamaFiles) {
  a = character()
  b = character()
  c = character()
  for (fileName in poamaFiles) {
    p = strsplit(fileName, "(_)|(.csv)|(e24)|^(emn)", fixed = FALSE, perl = TRUE)
    if(p[[1]][4] == "a" && is.na(p[[1]][6])) {
      a = c(a, fileName)
    }
    if(p[[1]][4] == "b" && is.na(p[[1]][6])) {
      b = c(b, fileName)
    }
    if(p[[1]][4] == "b" && is.na(p[[1]][6])) {
      c = c(c, fileName)
    }
  }
  return (list(a, b, c))
}

setwd(".")
poamaDir = 'data/POAMA_Aug_2013'
poamaFiles = list.files(file.path(getwd(), poamaDir))
poamaFiles = list.files(file.path(getwd(), poamaDir))

l = abc(poamaFiles)
print(l)