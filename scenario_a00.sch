1905          Starting year
2013          Last year
Gympie.100    Site file name
0             Labeling type
-1            Labeling year
-1.00         Microcosm
-1            CO2 Systems
-1            pH effect
-1            Soil Warming
0             N input scalar option
0             OMAD scalar option
-1            Climate scalar option
3             Initial system
TRC4          Initial crop
TRFR          Initial tree

Year Month Option
1             Block #   
1905          Last year
1             Repeats # years
1905          Output starting year
1             Output month
0.083         Output interval
F             Weather choice
longterm.wth
     1    1 CROP
  TRC4 
     1    1 FRST
     1  365 LAST
-999 -999 X  
2             Block #   
1974          Last year
1             Repeats # years
1906          Output starting year
1             Output month
0.083         Output interval
F             Weather choice
longterm.wth
     1    1 CROP
TRC4 
     1    1 FRST
     1    1 GRAZ
GL    
     1    31 GRAZ
GL
     1    61 GRAZ
GL    
     1    91 GRAZ
GL    
     1    121 GRAZ
GL     
     1    151 GRAZ
GL
     1    181 GRAZ
GL
     1    211 GRAZ
GL
     1	  241 GRAZ
GL
     1    271 GRAZ
GL
     1	  301 GRAZ
GL
     1    331 GRAZ
GL     
     1  365 LAST
-999 -999 X  
3             Block #   
1984          Last year
1             Repeats # years
1975          Output starting year
1             Output month
0.083         Output interval
F             Weather choice
longterm.wth
     1    1 CROP
TRC4 
     1    1 FRST
     1    1 GRAZ
GL    
     1    31 GRAZ
GL
     1    61 GRAZ
GL    
     1    91 GRAZ
GL    
     1    121 GRAZ
GL     
     1    151 GRAZ
GL
     1    181 GRAZ
GL
     1    182 FERT
SP10     	
     1    211 GRAZ
GL
     1	  241 GRAZ
GL
     1    271 GRAZ
GL
     1	  301 GRAZ
GL
     1    331 GRAZ
GL     
     1  365 LAST
-999 -999 X  
4             Block #   
1989          Last year
1             Repeats # years
1985          Output starting year
1             Output month
0.083         Output interval
F             Weather choice
longterm.wth
     1    1 CROP
TRC4 
     1    1 FRST
     1    1 GRAZ
GH_SU
     1    2 GRAZ
G_RES      
     1   14 GRAZ
GH_SU
     1   15 GRAZ
G_RES     
     1   28 GRAZ
GH_SU
     1   29 GRAZ
G_RES     
     1   42 GRAZ
GH_SU
     1   43 GRAZ
G_RES     
     1   56 GRAZ
GH_SU
     1   57 GRAZ
G_RES
     1   70 GRAZ
GH_SU
     1   71 GRAZ
G_RES
     1   84 GRAZ
GH_SU
     1   85 GRAZ
G_RES
     1   98 GRAZ
GH_SU
     1   99 GRAZ
G_RES
     1  112 GRAZ
GH_SU
     1  113 GRAZ
G_RES
     1  120 GRAZ
GH_SU
     1  121 LAST
     1  121 GRAZ  
GH_MU
     1  122 CROP
TRC4
     1  122 FRST
     1  122 FERT
DAP10
     1  165 GRAZ
GH_SU
     1  166 GRAZ
G_RES
     1  166 FERT
N4.5     
     1  186 GRAZ
GH_WI
     1  187 GRAZ
G_RES
     1  187 FERT
N4.5
     1  207 GRAZ
GH_WI
     1  208 GRAZ
G_RES
     1  208 FERT
N4.5
     1  228 GRAZ
GH_WI
     1  229 GRAZ
G_RES
     1  229 FERT
N4.5
     1  249 GRAZ
GH_WI
     1  250 GRAZ
G_RES
     1  250 FERT
N4.5
     1  270 GRAZ
GH_WI
     1  271 GRAZ
G_RES
     1  271 FERT
N4.5
     1  291 GRAZ
GH_WI
     1  292 GRAZ
G_RES
     1  292 FERT
N4.5
     1  312 GRAZ
GH_WI
     1  313 GRAZ
G_RES
     1  326 GRAZ
GH_WI
     1  327 GRAZ
G_RES
     1  340 GRAZ
GH_SU
     1  341 GRAZ
G_RES
     1  354 GRAZ
GH_SU
     1  355 GRAZ
G_RES
     1  365 LAST
-999 -999 X  
5             Block #   
2012          Last year
1             Repeats # years
1990          Output starting year
1             Output month
0.083         Output interval
F             Weather choice
1990.wth
     1    1 CROP
TRC4 
     1    1 FRST
     1    1 GRAZ
GH_SU
     1    2 GRAZ
G_RES  
     1   14 GRAZ
GH_SU
     1   15 GRAZ
G_RES     
     1   28 GRAZ
GH_SU
     1   29 GRAZ
G_RES     
     1   42 GRAZ
GH_SU
     1   43 GRAZ
G_RES     
     1   56 GRAZ
GH_SU
     1   57 GRAZ
G_RES
     1   70 GRAZ
GH_SU
     1   71 GRAZ
G_RES
     1   84 GRAZ
GH_SU
     1   85 GRAZ
G_RES
     1   98 GRAZ
GH_SU
     1   99 GRAZ
G_RES
     1  112 GRAZ
GH_SU
     1  113 GRAZ
G_RES
     1  120 GRAZ
GH_SU
     1  121 LAST
     1  121 GRAZ  
GH_MU
     1  122 CROP
TRC4
     1  122 FRST
     1  122 FERT
DAP10
     1  165 GRAZ
GH_SU
     1  166 GRAZ
G_RES
     1  166 FERT
N4.5     
     1  186 GRAZ
GH_WI
     1  187 GRAZ
G_RES
     1  187 FERT
N4.5
     1  207 GRAZ
GH_WI
     1  208 GRAZ
G_RES
     1  208 FERT
N4.5
     1  228 GRAZ
GH_WI
     1  229 GRAZ
G_RES
     1  229 FERT
N4.5
     1  249 GRAZ
GH_WI
     1  250 GRAZ
G_RES
     1  250 FERT
N4.5
     1  270 GRAZ
GH_WI
     1  271 GRAZ
G_RES
     1  271 FERT
N4.5
     1  291 GRAZ
GH_WI
     1  292 GRAZ
G_RES
     1  292 FERT
N4.5
     1  312 GRAZ
GH_WI
     1  313 GRAZ
G_RES
     1  326 GRAZ
GH_WI
     1  327 GRAZ
G_RES
     1  340 GRAZ
GH_SU
     1  341 GRAZ
G_RES
     1  354 GRAZ
GH_SU
     1  355 GRAZ
G_RES
     1  365 LAST
-999 -999 X
6             Block #   
2013          Last year
1             Repeats # years
2013          Output starting year
1             Output month
0.083         Output interval
F             Weather choice
2013_a_00.wth
     1    1 CROP
TRC4 
     1    1 FRST
     1    1 GRAZ
GH_SU
     1    2 GRAZ
G_RES     
     1   14 GRAZ
GH_SU
     1   15 GRAZ
G_RES     
     1   28 GRAZ
GH_SU
     1   29 GRAZ
G_RES     
     1   42 GRAZ
GH_SU
     1   43 GRAZ
G_RES     
     1   56 GRAZ
GH_SU
     1   57 GRAZ
G_RES
     1   70 GRAZ
GH_SU
     1   71 GRAZ
G_RES
     1   84 GRAZ
GH_SU
     1   85 GRAZ
G_RES
     1   98 GRAZ
GH_SU
     1   99 GRAZ
G_RES
     1  112 GRAZ
GH_SU
     1  113 GRAZ
G_RES
     1  116 LAST
     1  116 GRAZ
GH_MU
     1  117 CROP
TRC4
     1  117 PLTM
     1  178 GRAZ
GH_SU
     1  179 GRAZ
G_RES
     1  200 GRAZ
GH_WI
     1  201 GRAZ
G_RES
     1  213 FERT
N6.0 #high fertiliser 20 kg/ha	 
     1  220 GRAZ
GH_WI
     1  221 GRAZ
G_RES
     1  249 GRAZ
GH_WI
     1  250 GRAZ
G_RES
     1  270 GRAZ
GH_WI
     1  271 GRAZ
G_RES
     1  290 GRAZ
GH_WI
     1  291 GRAZ
G_RES
     1  311 GRAZ
GH_WI
     1  312 GRAZ
G_RES
     1  326 GRAZ
GH_WI
     1  327 GRAZ
G_RES
     1  338 GRAZ
GH_SU
     1  339 GRAZ
G_RES
     1  365 LAST
-999 -999 X  