# Moises Sacal Apr/2016
#
# This script extracts soilnm from each scenario_[A|B|C][0:10]
#
# Source this script to execute main(dayCentPath, "Scenario","A")

scenario = function(fileName) {
  table = read.table(fileName, header = TRUE)
  #print(dim(table)[1])
  vec = vector()
  ag243 = 0
  ag220 = 0
  ag219 = 0
  ag213 = 0
  for(index in 1:dim(table)[1]) {
    if((table$time[index] >= 1905 && table$time[index] < 2014)) {
      #print(paste(table$time[index], table$soilnm[index], sep=" "))
      #print(paste(table$soilnm[index], " - ", table$soilnm[index - 1]))
      #print(table$dayofyr[index])
      #print(typeof(table$dayofyr[index]))
      #print(table$dayofyr[index] == 213)
      if(table$dayofyr[index] == 213) {
        ag213 = table$aglivn[index];
      }
      if(table$dayofyr[index] == 219) {
        ag219 = table$aglivn[index];
      }
      if(table$dayofyr[index] == 220) {
        ag220 = table$aglivn[index];
      }
      if(table$dayofyr[index] == 243) {
        ag243 = table$aglivn[index];
        newAglivn = (ag243 - ag220) + (ag219 + ag213)
        vec = c(vec, newAglivn)
      }
    }
  }
  return(vec)
}

main = function(dirPath, currentM, modelType) {
  yearCol = vector()
  for(i in 1905:2013) {
    yearCol = c(yearCol, i)
  }
  frame = data.frame(year = yearCol)
  colnaming = c("years")
  for(index in 0:0) {
    if(index < 10){m = sprintf("0%d", index)} else {m = sprintf("%d", index)}
    fileName = paste(currentM, "_", tolower(modelType), m, ".bio", sep = "")
    scenarioPath = file.path(dirPath, paste(currentM, modelType, sep=""), fileName)
    vec = vector()
    vec = scenario(scenarioPath) 
    colnaming = c(colnaming, paste(currentM, "_", modelType, m, sep=""))
    frame = cbind(frame, vec)
  }
  colnames(frame) = colnaming
  write.csv(frame, file = file.path(dirPath, paste(currentM, modelType, "_bio_aglivnCALC.csv", sep = "")), row.names = FALSE)
}

#dirPath is the subpath containing all M[A|B|C] folders
dayCentPath = file.path("~","RProjects","ensemble01","data", "BIOS")
#dayCentPath = file.path("C:","Users","sacalbon","daycent", "BIOS")
main(dayCentPath, "scenario","A")
#main(dayCentPath, "Scenario","B")
#main(dayCentPath, "Scenario","C")
