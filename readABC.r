#!/usr/local/bin/R
abc = function(poamaFiles) {
  a = character()
  b = character()
  c = character()
  for (fileName in poamaFiles) {
    p = strsplit(fileName, "(_)|(.csv)|(e24)|^(emn)", fixed = FALSE, perl = TRUE)
    if(p[[1]][4] == "a" && is.na(p[[1]][6])) {
      a = c(a, fileName)
    }
    if(p[[1]][4] == "b" && is.na(p[[1]][6])) {
      b = c(b, fileName)
    }
    if(p[[1]][4] == "b" && is.na(p[[1]][6])) {
      c = c(c, fileName)
    }
  }
  return (list(a, b, c))
}
foo = function() {
  setwd(".")
  #read.csv("data/means_Aug.csv")->mean
  #wrap in year loop
  poamaDir = 'data/POAMA_Aug_2013'
  poamaFiles = list.files(file.path(getwd(), poamaDir))
  #print(file.path(getwd(), poamaDir))
  l = abc(poamaFiles)
  
  for (fileN in poamaFiles) {
    filePath = file.path(poamaDir, fileN)
    #foreach a, b, c
    #foreach abc00 -- abc10
    poama = read.csv(filePath, skip=8, nrows=31)
    for(i in 0:10) {
      rain_aug = (mean$Rain/10) + (poama$Rain.mm.day/10) 
      rain_aug_a = if(rain_aug < 0) 0 else rain_aug 
      #foreach abc00 -- abc10
      mintemp_aug_a = (mean$Mintemp) + (poama$T.min)
      #foreach abc00 -- abc10
      maxtemp_aug_a<-(mean$Maxtemp) + (poama$T.max)
    }
  }
}

foo()