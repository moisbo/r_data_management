# Moises Sacal Apr/2016
#
# Script that splits files contained in "POAMA all years" directory 
# into directory years starting with POAMA_XXXX
#
# Source this script to execute dirWD()
#
homePath = file.path("~","RProjects","ensemble01","data")
#homePath = file.path("C:","Users","username","data") #Windows example

setwd(homePath)
dirWD = file.path(homePath, "POAMA all years")
#separates a dir of files into multiple dirs of years
splitDirs = function(dirWD) {
  files = list.files(dirWD)
  dirList = vector()
  for (fileName in files) {
    p = strsplit(fileName, "([\\d]{4}_)|(_)|(.csv)|(e24)|^(emn)", fixed = FALSE, perl = TRUE)
    dirList = c(dirList, p[[1]][2])
  }
  dirList = unique(dirList)
  #create dirs according to the unique dirList
  for(ps in dirList) {
    dirName = paste("POAMA_", ps, sep = "")
    dir.create(file.path(homePath,"years", dirName), showWarnings = FALSE)
    filesSplit = vector()
    
    for (fileName in files) {
      pattern = paste("IASAd_", ps, sep="")
      p = grepl(pattern, fileName)
      if(p) {
        #if found of the same year as directory then add to filesSplit
        filesSplit = c(filesSplit, fileName)
      }
    }
    filesSplit = unique(filesSplit)
    filestocopy = file.path(filesSplit)
    targetdir = file.path(homePath, "years", paste("POAMA_", ps, sep = ""))
    #copy each filesSplit into the target directory
    for(filetocopy in filesSplit) {
       filetocopy = file.path(dirWD, filetocopy)
       file.copy(from = filetocopy, to = targetdir)
    }
  }
}

splitDirs(dirWD)