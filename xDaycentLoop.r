# Moises Sacal Apr/2016
#
# This script executes dailycent and daylicent_list models in a loop for 
# folders containing files starting with scenario_[A|B|C][0:10]
#
# Source this script to execute main(dayCentPath, "scenario", "a")

xLoop = function(dayCentPath, index, currentM, m, resFolder) {
  model = paste(currentM, "_", m, index, sep = "")
  
  #del MA_00.bin
  mabin = file.path(getwd(), paste(model, ".bin", sep = ""))
  if (file.exists(mabin)) file.remove(mabin)
  #del MA_00.lis
  malis = file.path(getwd(), paste(model, ".lis", sep = ""))
  if (file.exists(malis)) file.remove(malis)
  #dailydaycent -s MA_00 -n MA_00 -e equilibrium
  arg = paste("-s ", model, " -n ", model, " -e equilibrium", sep = "")
  status = xCmd("DailyDayCent.exe", arg)
  #dailydaycent_list100 MA_00 MA_00 outvars.txt
  arg = paste(" ", model, " ", model, " outvars.txt", sep = "")
  status = xCmd("DailyDayCent_list100.exe", arg)
  #Get .lis put in .lis folder
  lisFile = paste(model, ".lis", sep = "") 
  moveFile(file.path(getwd(), lisFile),
           file.path(resFolder, lisFile))
  #Get bio.out put in bio.out folder
  bioFile = "bio.out"
  #file.copy(from = file.path(dayCentPath, bioFile), to = file.path(resFolder, paste(model, ".bio", sep = "")))
  moveFile(file.path(getwd(), bioFile),
           file.path(resFolder, paste(model, ".bio", sep = "")))              
}

moveFile = function(from, to) {
  todir = dirname(to)
  if (!isTRUE(file.info(todir)$isdir)) dir.create(todir, recursive=TRUE)
  file.rename(from = from,  to = to)
}

xCmd = function(cmd, cmdArgs) {
  print(paste(cmd, cmdArgs, sep = " "))
  cmdRes = system2(cmd, cmdArgs, wait = TRUE, stderr = TRUE, stdout = TRUE)
  if(!is.null(attr(cmdRes, "status"))) {
    print(cmdRes)
    print("===================error===================")
  } else {
    print(cmdRes)
  }
  return (cmdRes)
}

main = function(dayCentPath, currentM, modelType) {
  for(index in 0:10) {
    if(index < 10){
      i = sprintf("0%d", index)
    }else {
      i = sprintf("%d", index)
    }
    setwd(file.path(dayCentPath, paste(currentM, toupper(modelType), sep = "")))
    resFolder = file.path(dayCentPath, "result", paste(currentM, modelType, sep=""))
    if(!dir.exists(resFolder)) dir.create(resFolder, showWarnings = FALSE)
    xLoop(dayCentPath, i, currentM, modelType, resFolder)
  }
}

#dayCentPath is the subpath containing all M[A|B|C] folders
#dayCentPath = file.path("E:","QUT","Postdoc_IFE","Daycent_new","Gympie_Forecasts_test_script")
dayCentPath = file.path("C:","Users","sacalbon","daycent")

main(dayCentPath, "scenario", "a")
main(dayCentPath, "scenario", "b")
main(dayCentPath, "scenario", "c")