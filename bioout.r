# Moises Sacal Apr/2016
#
# This script extracts aglivc and aglivn from each scenario_[A|B|C]_.bio 
#
# Source this script to execute main(dayCentPath, "Scenario","A")

bioout = function(fileName) {
  table = read.table(fileName, header = TRUE)
  #print(dim(table)[1]) #size of table
  resbio = data.frame()
  frame = data.frame()
  monthly = 0
  for(index in 1:dim(table)[1]) {
    if(grepl("\\d{4}.58", table$time[index]) && (table$time[index] >= 1905 && table$time[index] < 2014)) { #change years
      monthly = monthly + 1
      frame = rbind(frame, data.frame(as.integer(table$time[index]), table$dayofyr[index], table$aglivc[index], table$aglivn[index]))
      if(monthly == 31) {
        #in here get the dayofyr 
        #yieldC, yieldN = (220-213) + (243-221)
        #if leap()? not sure...
        resbio = rbind(resbio, frame)
        frame = data.frame()
        monthly = 0
      }
    }
  }
  return(resbio)
}

main = function(dirPath, currentM, modelType) {
  for(index in 0:10) {
    if(index < 10){m = sprintf("0%d", index)} else {m = sprintf("%d", index)}
    fileName = paste(currentM, "_", tolower(modelType), m, ".bio", sep = "")
    frame = bioout(file.path(dirPath, paste(currentM, modelType, sep = ""), fileName))
    colnames(frame) = c("year", "dayofyr", "aglivc", "aglivn")
    write.csv(frame, file = file.path(dirPath, paste(currentM, modelType, "_bio.csv", sep = "")), row.names = FALSE)
  }
}

#dirPath is the subpath containing all M[A|B|C] folders
dayCentPath = file.path("C:","Users","sacalbon","daycent", "result")
main(dayCentPath, "Scenario", "A")
#main(dayCentPath, "Scenario", "B")
#main(dayCentPath, "Scenario", "C")