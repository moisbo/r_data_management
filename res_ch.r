# Moises Sacal Apr/2016
# 
# Script that modifies .RES files in a directory switching the chamber (chA) name and the sample [1:4] 
# this replaces the chA for chB in each index 

modifyLines = function(dirPath, chA, chB) {
  #find .RES files
  files = list.files(path = dirPath, pattern = "\\.RES$")
  for (fileName in files) {
    print(fileName)
    cFile = file.path(dirPath, fileName)
    aCsv = tryCatch({read.csv(cFile, header = F, skip = 21, stringsAsFactors = F)}
                    ,error = function(error) {
                      print(paste("Error in file", cFile))
                      aCsv = NULL
                    })
    if(!is.null(aCsv)) {
      for(index in 1:nrow(aCsv) ) {
        cs = toString(aCsv[[3]][index])
        if(!is.null(grepl("cs-\\d", cs))){ #because R
          if(!grepl("cs-\\d", cs)) {
            for(i in 1:48) {
              if(chA[i] == cs) {
                aCsv[[3]][index] = chB[i]
              }
            }
          }
        }
      }
    }
    con = file(cFile, blocking = F)
    header = readLines(con)
    close(con)
    cat("", file = cFile, append = F, sep = "") #clear file
    for(i in 1:21){
      cat(header[[i]], file = cFile, append = T, sep = "\n")
    }
    write.table(aCsv, file = cFile, sep = ",", col.names = F, row.names = F, append = T)
  }
}

dirPath = file.path("~","Downloads","Backwards_comp")
#dirPath = file.path("C:","Users","username","data") #Windows example
options(warn=1) #show warnings
#chANot = c("1-1","1-2","1-3","1-4","5-1","5-2","5-3","5-4","9-1","9-2","9-3","9-4")
chOriginal = c("1-1","2-1","3-1","4-1",
               "1-2","2-2","3-2","4-2",
               "1-3","2-3","3-3","4-3",
               "1-4","2-4","3-4","4-4",
               "5-1","6-1","7-1","8-1",
               "5-2","6-2","7-2","8-2",
               "5-3","6-3","7-3","8-3",
               "5-4","6-4","7-4","8-4",
               "9-1","10-1","11-1","12-1",
               "9-2","10-2","11-2","12-2",
               "9-3","10-3","11-3","12-3",
               "9-4","10-4","11-4","12-4")
chReplace = c("1-1","1-2","1-3","1-4",
              "2-1","2-2","2-3","2-4",
              "3-1","3-2","3-3","3-4",
              "4-1","4-2","4-3","4-4",
              "5-1","5-2","5-3","5-4",
              "6-1","6-2","6-3","6-4",
              "7-1","7-2","7-3","7-4",
              "8-1","8-2","8-3","8-4",
              "9-1","9-2","9-3","9-4",
              "10-1","10-2","10-3","10-4",
              "11-1","11-2","11-3","11-4",
              "12-1","12-2","12-3","12-4")
modifyLines(dirPath, chOriginal, chReplace)
