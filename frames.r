#partial script extracted from ensemble02.r
#used to arrange forecast vectors into a single file
#with forecast object extract the order needed for each, i.e. rainFrameA = extract only rain and cbind it to rainFrame
rainFrameA = vector()
for(col in forecast@pmAL[]) {
  rainFrameA = cbind(rainFrameA, col@rain_aug)
}
rainFrameB = vector()
for(col in forecast@pmBL[]) {
  rainFrameB = cbind(rainFrameB, col@rain_aug)
}
rainFrameC = vector()
for(col in forecast@pmCL[]) {
  rainFrameC = cbind(rainFrameC, col@rain_aug)
}
########
mintempFrameA = vector()
for(col in forecast@pmAL[]) {
  mintempFrameA = cbind(mintempFrameA, col@mintemp_aug)
}
mintempFrameB = vector()
for(col in forecast@pmBL[]) {
  mintempFrameB = cbind(mintempFrameB, col@mintemp_aug)
}
mintempFrameC = vector()
for(col in forecast@pmCL[]) {
  mintempFrameC = cbind(mintempFrameC, col@mintemp_aug)
}
#######
maxtempFrameA = vector()
for(col in forecast@pmAL[]) {
  maxtempFrameA = cbind(maxtempFrameA, col@maxtemp_aug)
}
maxtempFrameB = vector()
for(col in forecast@pmBL[]) {
  maxtempFrameB = cbind(maxtempFrameB, col@maxtemp_aug)
}
maxtempFrameC = vector()
for(col in forecast@pmCL[]) {
  maxtempFrameC = cbind(maxtempFrameC, col@maxtemp_aug)
}
#combine all columns into a single frame
frame = data.frame(col@date_aug,
                   rainFrameA, 
                   rainFrameB,
                   rainFrameC,
                   mintempFrameA,
                   mintempFrameB,
                   mintempFrameC,
                   maxtempFrameA,
                   maxtempFrameB,
                   maxtempFrameC
)
poamaRes = paste(poamaRes, ".csv", sep = "")
#write each frame for each year
write.csv(frame, file=poamaRes)