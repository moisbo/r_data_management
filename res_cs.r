# Moises Sacal Apr/2016
# 
# Script that modifies .RES files in a directory switching the calibration for the chamber and sample for chamber

modifyLines = function(dirPath) {
  #find .RES files
  files = list.files(path = dirPath, pattern = "\\.RES$")
  for (fileName in files) {
    print(fileName)
    cFile = file.path(dirPath, fileName)
    aCsv = read.csv(cFile, header = F, skip = 21, stringsAsFactors = F)
    for(index in 1:nrow(aCsv) ) {
      cs = toString(aCsv[[3]][index])
      if(grepl("cs-\\d", cs)) {
        currentCalibration = strsplit(cs, "-")[[1]][2]
        currentSample = 0
      }
      if(!is.null(grepl("cs-\\d", cs))){ #because R
        if(!grepl("cs-\\d", cs)) {
          str = strsplit(cs, "-")
          currentSample = currentSample + 1
          sample = currentSample
          chamber = currentCalibration
          aCsv[[3]][index] = paste(chamber, sample, sep="-")
        }
      }
    }
    con = file(cFile, blocking = F)
    header = readLines(con)
    close(con)
    cat("", file = cFile, append = F, sep = "") #clear file
    for(i in 1:21){
      cat(header[[i]], file = cFile, append = T, sep = "\n")
    }
    write.table(aCsv, file = cFile, sep = ",", col.names = F, row.names = F, append = T)
  }
}

dirPath = file.path("~","Downloads","Backwards_comp")
#dirPath = file.path("C:","Users","username","data") #Windows example
options(warn=1) #show warnings
modifyLines(dirPath)
