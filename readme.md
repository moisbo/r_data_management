---
R script for integrating modell ensembles into Daycent
---

Example:
```
yearly("Gympie_climate.csv", "means_Aug.csv")
```

Each model is hardwired into the for loop of eachYear (so we see progress faster), for example if needed model B change: pmAL to pmBL and modelType to B
```
for(col in forecast@pmBL) {
...
modelType = "B"
...
}
```

## Data Folder
homePath is data/

get data folders, execute splitDirs.r, then run yearly in ensemble02.r

years folder is in data/years/
